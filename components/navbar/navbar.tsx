import classNames from 'classnames';
import Link from 'next/link';
import { Container, Nav, Navbar } from 'react-bootstrap';
import { When } from 'react-if';
import ExternalLinkIcon from '../utils/external-link-icon';
import Logo from './logo';
import styles from "./navbar.module.scss";

export interface NavbarComponentProps extends React.HTMLAttributes<HTMLDivElement> {
  bg?: string,
  variant?: string,
  logoVariant?: "dark" | "light" | "mono",
  addPadders?: boolean
}

const defaultProps = {
  addPadders: true,
  logoVariant: "dark",
  variant: "dark"
}

function NavbarComponent({ bg, variant: variant, addPadders, logoVariant, className }: NavbarComponentProps & typeof defaultProps) {
  return (
    <>
      <Navbar bg={bg} variant={variant} expand="lg" className={classNames([className])} fixed='top'>
        <Container>
          <Link href="/" passHref legacyBehavior>
            <Navbar.Brand aria-label='Home'>
              <Logo className={styles.logo} variant={logoVariant} />
            </Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls="main-navbar" />
          <Navbar.Collapse id="main-navbar">
            <Nav className="ms-auto gap-4">
              <Link href="https://blog.christianto.net/?utm_source=home&utm_medium=navbar" passHref legacyBehavior>
                <Nav.Link>Blog <ExternalLinkIcon /></Nav.Link>
              </Link>
              <Link href="/works" passHref legacyBehavior>
                <Nav.Link>Works</Nav.Link>
              </Link>
              <Link href="/about" passHref legacyBehavior>
                <Nav.Link>About</Nav.Link>
              </Link>
              <Link href="/contact" passHref legacyBehavior>
                <Nav.Link>Contact</Nav.Link>
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <When condition={addPadders}>
        <div className={classNames([styles.padders, className])} />
      </When>
    </>
  )
}

NavbarComponent.defaultProps = defaultProps;

export default NavbarComponent
