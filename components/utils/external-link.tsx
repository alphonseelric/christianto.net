import Link, { LinkProps } from 'next/link'
import React from 'react'
import ExternalLinkIcon from './external-link-icon'

export function ExternalLink({ children, href, ...props }: (LinkProps & React.HTMLAttributes<HTMLAnchorElement>)) {
    return (
        <Link target='_blank' rel='noopener noreferrer' href={href} {...props}>{children} <ExternalLinkIcon /></Link>
    )
}

export default ExternalLink
