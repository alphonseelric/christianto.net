import { faGitlab, faLinkedin, faMastodon, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import Link from 'next/link';
import { Badge, Button, ButtonGroup, Col, Container, Nav, NavLink, Row } from 'react-bootstrap';
import ExternalLinkIcon from '../utils/external-link-icon';

import styles from "./footer.module.scss";

function FooterComponent() {
  return (
    <>
      <footer className={styles.footer}>
        <Container>
          <Row>
            <Col xs={12} md={4}>
              <div className="my-3">
                <div className="fs-2 fw-bold">chez14</div>
                <p>Software engineer in <del>Bandung</del> area.</p>
              </div>

              <div className="my-3">
                <address className='text-muted' title={"These are here to fill in the blank, the address are obviously fake.\n Earth-based address are available by appointment only."}>
                  Pod 11, Algoramroh Street, <br />
                  Sector 140, Planet Mars. <br />
                  <small className="font-monospace">FCAB-EJGY</small>
                </address>
                <div>
                  <FontAwesomeIcon icon={faEnvelope} /> : hi (at) this domain
                </div>
              </div>

              <div className="my-5">
                <ButtonGroup role="group" aria-label="Chris's Social Media" className="gap-3">
                  <Link passHref legacyBehavior href="https://gitlab.com/chez14">
                    <Button variant="dark" target="_blank" rel="noopener noreferrer" aria-label='GitLab'><FontAwesomeIcon icon={faGitlab} /></Button>
                  </Link>
                  <Link passHref legacyBehavior href="https://linkedin.com/in/chez14">
                    <Button variant="dark" target="_blank" rel="noopener noreferrer" aria-label='LinkedIn'><FontAwesomeIcon icon={faLinkedin} /></Button>
                  </Link>
                  <Link passHref legacyBehavior href="https://twitter.com/heychez14">
                    <Button variant="dark" target="_blank" rel="noopener noreferrer" aria-label='Twitter'><FontAwesomeIcon icon={faTwitter} /></Button>
                  </Link>
                  <Link passHref legacyBehavior href="https://mastodon.online/@chez14">
                    <Button variant="dark" target="_blank" rel="noopener noreferrer me" aria-label='Mastodon (Mastodon.online)'><FontAwesomeIcon icon={faMastodon} /></Button>
                  </Link>
                  <Link passHref legacyBehavior href="https://youtube.com/@chez14">
                    <Button variant="dark" target="_blank" rel="noopener noreferrer" aria-label='YouTube'><FontAwesomeIcon icon={faYoutube} /></Button>
                  </Link>
                </ButtonGroup>
              </div>
            </Col>
            <Col xs={12} md={2}>
              <div className="my-3">
                <p className="h5">My Thing</p>
                <Nav className={classNames(["flex-column", styles.footerNavs])}>
                  <Link passHref legacyBehavior href="/about">
                    <NavLink>About</NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="https://blog.christianto.net/">
                    <NavLink>Blog <ExternalLinkIcon /></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="/works">
                    <NavLink>Works <Badge bg="secondary">WIP</Badge></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="/contact">
                    <NavLink>Contact <Badge bg="secondary">WIP</Badge></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="/links">
                    <NavLink>Links Directory</NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="https://blog.christianto.net/pgp">
                    <NavLink>PGP Key <ExternalLinkIcon /></NavLink>
                  </Link>
                </Nav>
              </div>
            </Col>
            <Col xs={12} md={2}>
              <div className="my-3">
                <p className="h5">Projects</p>
                <Nav className={classNames(["flex-column", styles.footerNavs])}>
                  <Link passHref legacyBehavior href="https://gitlab.com/chez14/christianto.net">
                    <NavLink target="_blank" rel="noopener noreferrer">Christianto.net <ExternalLinkIcon /></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="https://gitlab.com/chez14/publisher">
                    <NavLink target="_blank" rel="noopener noreferrer">Publisher <ExternalLinkIcon /></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="https://gitlab.com/chez14/f3-ilgar">
                    <NavLink target="_blank" rel="noopener noreferrer">F3-Ilgar <ExternalLinkIcon /></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="https://github.com/chez14/indonesian-standards">
                    <NavLink target="_blank" rel="noopener noreferrer">Indonesian Standards <ExternalLinkIcon /></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="https://github.com/chez14/gratheus">
                    <NavLink target="_blank" rel="noopener noreferrer">Gratheus <ExternalLinkIcon /></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="/works">
                    <NavLink>More...</NavLink>
                  </Link>
                </Nav>
              </div>
            </Col>
            <Col xs={12} md={2}>
              <div className="my-3">
                <p className="h5">Find me in the wild</p>
                <Nav className={classNames(["flex-column", styles.footerNavs])}>
                  <Link passHref legacyBehavior href="https://gitlab.com/chez14">
                    <NavLink target="_blank" rel="noopener noreferrer">chez&apos;s GitLab <ExternalLinkIcon /></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="https://github.com/chez14">
                    <NavLink target="_blank" rel="noopener noreferrer">chez&apos;s GitHub <ExternalLinkIcon /></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="https://gitlab.com/digivert">
                    <NavLink target="_blank" rel="noopener noreferrer">Digivert Asmasta <ExternalLinkIcon /></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="https://gitlab.com/net.christianto">
                    <NavLink target="_blank" rel="noopener noreferrer">chez&apos;s GitLab Group <ExternalLinkIcon /></NavLink>
                  </Link>
                  <Link passHref legacyBehavior href="/links">
                    <NavLink>More...</NavLink>
                  </Link>
                </Nav>
              </div>
            </Col>

          </Row>
        </Container>
      </footer>
      <footer className={styles.footerCopyright}>
        <Container className="small">
          <Row>
            <Col xs={12} md={6} className="footer-final-copy">
              <p>
                Copyright ©2020-{(new Date()).getFullYear()} Gunawan &ldquo;chez14&rdquo; Christianto.
              </p>
            </Col>
            <Col xs={12} md={6} className="footer-final-notice">
              <p>
                Made with 💙 on Nextjs, Bootstrap, <Link href="/about#about-site">and more</Link>. See the source on <Link href="https://gitlab.com/chez14/christianto.net" target="_blank" rel="noopener noreferrer">GitLab <ExternalLinkIcon /></Link>.
              </p>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  )
}

export default FooterComponent
