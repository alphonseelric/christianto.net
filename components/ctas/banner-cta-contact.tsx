import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import Link from 'next/link';
import { Button, Col, Row } from 'react-bootstrap';
import styles from "./banner-cta-contact.module.scss";

export interface CtaContactProps extends React.HTMLAttributes<HTMLDivElement> {
  title?: string,
  description?: string,
  ctaVariant?: string,
  titleTag: keyof JSX.IntrinsicElements
}

export const defaultProps: CtaContactProps = {
  title: "Want me to create a specific solution for you?",
  description: "Good news: you can! Contact me to get started.",
  titleTag: "h3",
  ctaVariant: "outline-light"
}


function CtaContact({ title, description, titleTag, ctaVariant, className }: CtaContactProps & typeof defaultProps) {
  const ProxyElementTitle = titleTag;

  return (
    <div className={classNames([styles.ctaContact, className])}>
      <Row className="align-items-center">
        <Col xs={12} md={6} className={styles.ctaText}>
          <ProxyElementTitle>{title}</ProxyElementTitle>
          <p>{description}</p>
        </Col>
        <Col xs={12} md={6} className={styles.ctaButton}>
          <Link href="/contact" passHref legacyBehavior>
            <Button variant={ctaVariant} size="lg">
              Contact Me <FontAwesomeIcon icon={faArrowRight} />
            </Button>
          </Link>
        </Col>
      </Row>
    </div>
  )
}

export default CtaContact

CtaContact.defaultProps = defaultProps;
