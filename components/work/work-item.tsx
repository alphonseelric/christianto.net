import { faGithub, faGitlab } from '@fortawesome/free-brands-svg-icons';
import { faHashtag } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import React from 'react'
import { Badge, Button, Card } from 'react-bootstrap'
import { When } from 'react-if';
import { WorkItem } from '~/data/works/model/works'
import styles from "./work-item.module.scss";


export interface WorkItemProps {
  work: WorkItem,
  detailLink?: string
}

function WorkItemComponent({ work, detailLink }: WorkItemProps) {

  let repoIcon = faGitlab;

  if (work.repoUrl?.repoType == "github") {
    repoIcon = faGithub;
  }

  return (
    <Card className={styles.workItem}>
      <Card.Body>
        <div className="card-work">
          <div className="card-work-icon">
            <span className="work-icon">{work.slug[0]}</span>
          </div>
          <div className="card-work-name">
            <Card.Title>{work.title}</Card.Title>
            {work.links?.slice(0, 3).map((link) => <a key={link.url} href={link.url} rel="noreferrer noopener" target="_blank" className='d-inline-block mx-2'>{link.title}</a>)}
          </div>
        </div>

        <When condition={!!work.tags}>
          <div className="work-tags mt-3">
            {work.tags?.slice(0, 5).map((tag) => <Badge key={tag} pill bg="secondary"><FontAwesomeIcon icon={faHashtag} /> {tag}</Badge>)}
          </div>
        </When>

        <div className="work-description mt-3">
          {work.description}
        </div>

        <div className="work-cta mt-3">
          <When condition={!!detailLink}>
            <Link href={detailLink || ""} passHref legacyBehavior>
              <Button variant='outline-primary'>
                Learn More
              </Button>
            </Link>
          </When>
          <When condition={!!work.repoUrl}>
            <Button variant='dark' href={work.repoUrl.url} rel="noopener" target="_blank">
              <FontAwesomeIcon icon={repoIcon} /> See Repository
            </Button>
          </When>
        </div>
      </Card.Body>
    </Card>
  )
}

export default WorkItemComponent
