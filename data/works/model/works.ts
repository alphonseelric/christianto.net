
export interface WorkItem {
  title: string,
  slug: string,
  tags?: string[],
  description?: string,
  iconUrl?: string,
  iconEmoji?: string,
  repoUrl: WorkRepo,
  links?: WorkUrl[],
  techStack?: TechStack[]
}

export interface WorkUrl {
  title?: string
  url: string,
  kind: "repo" | "live" | "blog"
}

export interface WorkRepo extends WorkUrl {
  repoType: "gitlab" | "github",
  kind: "repo"
}

export interface TechStack {
  title: string,
  type: "lang" | "infrastructure" | "library" | "framework" | "protocol" | "other",
  icon?: string
}


export const knownLanguages: { [key: string]: TechStack } = {
  rust: { title: "Rust", type: "lang" },
  php: { title: "PHP", type: "lang" },
  js: { title: "JavaScript", type: "lang" },
  ts: { title: "TypeScript", type: "lang" },
  bash: { title: "Bash", type: "lang" },
  bat: { title: "Windows Batch", type: "lang" },
}

export const knownFramework: { [key: string]: TechStack } = {
  rust: { title: "Rust", type: "framework" },
}

export const knownJsLibraries: { [key: string]: TechStack } = {
  react: { title: "React", type: "library" },
  mobx: { title: "MobX", type: "library" },
}

export const knownRustLibraries: { [key: string]: TechStack } = {
  axum: { title: "Axum", type: "library" },
}

export const knownInfra: { [key: string]: TechStack } = {
  docker: { title: "Docker", type: "infrastructure" },
}
