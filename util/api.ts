import axios from "axios";

export const apiCaller = axios.create({
  baseURL: process.env.PORTMAFIA_ENDPOINT,
  responseType: "json",
  headers: { "Accept-Encoding": "gzip,deflate,compress" }
});
