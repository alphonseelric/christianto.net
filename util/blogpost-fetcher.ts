import { BlogPost, BlogPostRaw } from "~/data/blogs/blog-post";
import { apiCaller } from "./api";

export async function getBlogPosts(): Promise<BlogPost[]> {
  let resp = await apiCaller.get<BlogPostRaw>('api/post')
  return resp.data.data;
}
