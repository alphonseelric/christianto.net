-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Contact: https://gitlab.com/chez14/christianto.net/-/issues/
Expires: 2025-12-30T21:00:00.000Z
Encryption: https://gl-statis.konten.christianto.net/pgp/chris.pgp.asc
Encryption: https://pgp.mit.edu/pks/lookup?op=vindex&search=0x332D7EAFBCDB79A3
Preferred-Languages: en,id
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEECe1yA/Nm+10KRIoH2jYPnvdUP8wFAmOKZzoACgkQ2jYPnvdU
P8yhMhAAobHeFZDGUAjg/u6dqiHmoC7KhNuCjQ+J7IuOHzawb187hjO2sOgFKXDu
JC+CV1lsGeDwdTAeN7H1MPg9iIVGMStB1PYrYfPvcnmCZX12Y4Bqu8ufINE4iP8S
RwCOvzigTum0aIXYq86fvJJeatw1DL1gJURtTuNAyuPHWjOg+rg04q/jxWQL216w
UcgRkqMopqc587nvB+qHNW17fs1AuyDhV+wHDSEzYbExfChw0hFaxWsKwmAsoef+
tOLPKAK8g86oCK4dMHrrvkl4eA4bOG5QNpWHXuu6NHcTC6jG6YrOc6KYt0Nm+8KF
1oiA1FPzUwREMzyiixxG0qufvunoX3O6U3ymCs4M7UCTGK3e1TT+ECVH1lDMAI15
BDQyPdA8mm9oVEXjCzijaLZnR46iT5jnHjtdTZ9D7ByZLSnwcoKIeBS5G19uXn1i
14bYZyhzwPqETt0hRa8ixmBhoz0YWlbOFwnP/dAtU/gKa9aLXt9oI/ZFiNc4Domx
Fst6CMwK5EqKpUwmqA09k0j+utx28KdFuE9P5iJt/Nku6ZqqecoE2YED9N3SNp6T
xCb7HSnQxo48Rhs69iZ5JYwcpgKjLI4rP8h6C1CAvs6z7bm6iJ0FWyJjOg3+rD0o
Q4tdNcmsYM+t1DYFrE1J31d2j5q+WXHNd9qS4ArM7w8BPFEn+sA=
=ipNc
-----END PGP SIGNATURE-----
