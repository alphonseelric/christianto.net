import { GetStaticProps } from 'next'
import { BreadcrumbJsonLd, NextSeo } from 'next-seo'
import Link from 'next/link'
import { Alert, Col, Container, Row } from 'react-bootstrap'
import { getUrlBaseName } from '~/util/url-helper'
import { PageWithNavbarProps } from './_app'
import profilePict from "~/assets/home/profile.jpg";

function ContactPage() {

  let urlBase = getUrlBaseName();

  return (
    <>
      <NextSeo
        title="Contact Chris"
        description="Have something to discuss with Chris? Contact him here!"
        openGraph={{
          images: [
            {
              url: `${urlBase.origin}${profilePict.src}`,
              width: profilePict.width,
              height: profilePict.height,
              alt: "An image of Chris in anime style"
            }
          ]
        }}
      />
      <BreadcrumbJsonLd
        itemListElements={[
          {
            position: 0,
            name: "Home",
            item: `${urlBase.origin}`
          },
          {
            position: 1,
            name: "Contact",
            item: `${urlBase.origin}/contact`
          }
        ]} />


      <div className="my-3">
        <Container>
          <h1>Contact Me</h1>
        </Container>
      </div>

      <Container>
        <div className="my-5">
          <Row>
            <Col xs={12} md={8}>
              <h2>Fill up a form, and I&apos;ll send you a reply</h2>
              <p>
                Please remember: I can only talk in English and Bahasa Indonesia,
                if you send me email from other language, your message will be
                translated via Google Translate, and I&apos;ll reply in English.
              </p>
              <Alert variant="info" className="my-4">
                <Alert.Heading>Under Construction</Alert.Heading>
                Unfortunately this feature is currently under construction, you can track its progress on
                issue <a href="https://gitlab.com/chez14/christianto.net/-/issues/13" target="_blank" rel="noopener noreferrer">#13</a>. <br />
                To contact Chris, please reach out via Email or other social media links provided on the Link Directory page. <br />
                Thank you!
              </Alert>
            </Col>
            <Col xs={12} md={4}>
              <h2>Other contact?</h2>
              <p>
                Email: <code>hi (at) this domain</code>. <br />
                Discord: <i>tbd</i>. <br />
                Find me anywhere in <Link href="/links">my link directories page</Link>.
              </p>
            </Col>
          </Row>
        </div>
      </Container>
    </>
  )
}

export default ContactPage

export const getStaticProps: GetStaticProps<PageWithNavbarProps> = async (ctx) => {
  return {
    props: {
      navbarProps: {
        addPadders: true,
        bg: "dark",
        logoVariant: "dark"
      }
    }
  }
}
