import { config } from '@fortawesome/fontawesome-svg-core'
import { DefaultSeo } from 'next-seo'
import type { AppProps, NextWebVitalsMetric } from 'next/app'
import { event, GoogleAnalytics } from 'nextjs-google-analytics'
import FooterComponent from '~/components/footer/footer'
import NavbarComponent, { NavbarComponentProps } from '~/components/navbar/navbar'
import '../styles/globals.scss'
config.autoAddCss = false

export interface PageWithNavbarProps {
  navbarProps?: NavbarComponentProps
}

export interface CustomAppProps extends AppProps {
  pageProps: any & PageWithNavbarProps
}

function CustomApp({ Component, pageProps }: CustomAppProps) {
  const { navbarProps, ...originalProps } = pageProps;
  return <>
    <DefaultSeo
      defaultTitle='Christianto'
      titleTemplate='%s | Christianto'
      twitter={{
        cardType: "summary_large_image",
        site: "heychez14"
      }}
      themeColor="#ee2737"
      openGraph={{
        type: "website",
        locale: "en",
        siteName: "Christianto"
      }}
    />

    <GoogleAnalytics trackPageViews strategy='lazyOnload' />

    <NavbarComponent {...navbarProps} />
    <Component {...originalProps} />
    <FooterComponent />
  </>
}

export default CustomApp
