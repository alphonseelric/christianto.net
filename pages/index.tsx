import type { GetServerSideProps } from 'next';
import { BreadcrumbJsonLd, LogoJsonLd, NextSeo } from 'next-seo';
import { Container } from 'react-bootstrap';
import profilePict from "~/assets/home/profile.jpg";
import { BlogPost } from '~/data/blogs/blog-post';
import HomeMainBanner from '~/layout/home/main-banner';
import HomeSectionBlogPost from '~/layout/home/section-blog-post';
import HomeSectionContact from '~/layout/home/section-contact';
import HomeSectionLatestProject from '~/layout/home/section-latest-project';
import { getBlogPosts } from '~/util/blogpost-fetcher';
import { getUrlBaseName } from '~/util/url-helper';
import { PageWithNavbarProps } from './_app';


export interface IndexPageProps {
  blogs: BlogPost[],
}

function HomePage({ blogs }: IndexPageProps) {

  let urlBase = getUrlBaseName();
  return (<>
    <NextSeo
      title=""
      description="Hi there! This is Chris, also known as chez14 around the internet. He's currently working as Maintenance Manager on DNArtworks Indonesia. He mostly works with web stuff, especially backend."
      openGraph={{
        images: [
          {
            url: `${urlBase.origin}${profilePict.src}`,
            width: profilePict.width,
            height: profilePict.height,
            alt: "An image of Chris in anime style"
          }
        ]
      }}
    />
    <LogoJsonLd
      type='Organization'
      logo="https://gl-statis.konten.christianto.net/logo/light-ver@0.5x.png"
      // Honestly... let's just hard-code this url.
      url="https://christianto.net"
    />
    <BreadcrumbJsonLd
      itemListElements={[
        {
          position: 0,
          name: "Home",
          item: urlBase.origin
        }
      ]} />

    <section className="mb-5">
      <HomeMainBanner />
    </section>
    <Container>
      <div className="my-5">
        <HomeSectionLatestProject />
      </div>

      <div className="my-5">
        <HomeSectionBlogPost blogs={blogs} />

      </div>

      <div className="my-5">
        <HomeSectionContact />
      </div>
    </Container>
  </>);
}

export default HomePage

export type IndexStaticProps = PageWithNavbarProps | IndexPageProps;

export const getServerSideProps: GetServerSideProps<IndexStaticProps> = async (ctx) => {
  let blogPosts = await getBlogPosts() ?? [];
  blogPosts = blogPosts.slice(0, 7);

  return {
    props: {
      blogs: blogPosts,
      navbarProps: {
        addPadders: false,
        bg: "dark",
        logoVariant: "dark"
      }
    }
  }
}
