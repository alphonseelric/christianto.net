import { Gitlab } from '@gitbeaker/browser';
import { GetServerSideProps } from 'next';
import { NextSeo } from 'next-seo';
import { Alert, Col, Container, Row } from 'react-bootstrap';
import { remark } from 'remark';
import remarkHtml from 'remark-html';

import { workList } from "~/data/works/list";
import { WorkItem } from '~/data/works/model/works';
import { PageWithNavbarProps } from '../_app';


export interface WorksIndexPageProps {
  works: WorkItem[],
  issueContent: string
}

function WorksIndexPage({ works, issueContent }: WorksIndexPageProps) {
  return (<>
    <NextSeo title='Works' description="Chris's works and projects over the years." />

    <Container>
      <div className="my-3">
        <h1>Works and Projects</h1>
      </div>
    </Container>

    <Container>
      <div className="my-5">
        <Row>
          <Col xs={12} md={8}>
            <Alert variant='info'>
              <Alert.Heading>Under Construction</Alert.Heading>
              <p>
                This page still WIP, track the development on
                Issue <a href="https://gitlab.com/chez14/christianto.net/-/issues/12" target="_blank" rel="noopener noreferrer">#12</a>.
                For now, please enjoy the following list.
              </p>
            </Alert>
          </Col>
        </Row>
      </div>

      <div className="my-5">
        <Row>
          <Col xs={12} md={8}>
            <div dangerouslySetInnerHTML={{ __html: issueContent }}></div>
          </Col>
        </Row>
      </div>

    </Container>
  </>
  )
}

export default WorksIndexPage

export type WorksIndexPagePropsInternal = WorksIndexPageProps | PageWithNavbarProps;

export const getServerSideProps: GetServerSideProps<WorksIndexPagePropsInternal> = async (ctx) => {
  let projectId = 24281566;
  let issueIid = 11;

  let gl = (new Gitlab({}));
  let issueContent = await gl.Issues.show(projectId, issueIid);

  let processedContent = await remark()
    .use(remarkHtml)
    .process(issueContent.description);

  return {
    props: {
      works: workList,
      issueContent: processedContent.toString(),
      navbarProps: {
        bg: "dark",
        logoVariant: "dark"
      }
    }
  }
}
