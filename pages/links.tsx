import { GetStaticProps } from 'next'
import { BreadcrumbJsonLd, NextSeo } from 'next-seo'
import { Container } from 'react-bootstrap'
import profilePict from "~/assets/home/profile.jpg"
import CtaContact from '~/components/ctas/banner-cta-contact'
import LinksPageContent1 from '~/layout/links/content-1-intro'
import LinkPageContent2 from '~/layout/links/content-2-linkies'
import LinksPageMainBanner from '~/layout/links/main-banner'
import styles from "~/styles/page/links.module.scss"
import { getUrlBaseName } from '~/util/url-helper'
import { PageWithNavbarProps } from './_app'


function LinksPage() {
  // to import the styles.
  styles;

  let urlBase = getUrlBaseName();
  return (
    <>
      <NextSeo
        title="Links"
        description="Find Chris everywhere anywhere! Connect with him to see what he's being up to."
        openGraph={{
          images: [
            {
              url: `${urlBase.origin}${profilePict.src}`,
              width: profilePict.width,
              height: profilePict.height,
              alt: "An image of Chris in anime style"
            }
          ]
        }}
      />

      <BreadcrumbJsonLd
        itemListElements={[
          {
            position: 0,
            name: "Home",
            item: `${urlBase.origin}`
          },
          {
            position: 1,
            name: "Links",
            item: `${urlBase.origin}/links`
          }
        ]} />


      <LinksPageMainBanner />

      <div className="my-5">
        <LinksPageContent1 />
      </div>

      <div className="my-5">
        <LinkPageContent2 />
      </div>

      <div className="my-5">
        <Container>
          <CtaContact />
        </Container>
      </div>
    </>
  )
}

export default LinksPage


export const getStaticProps: GetStaticProps<PageWithNavbarProps> = async (ctx) => {
  return {
    props: {
      navbarProps: {
        isSchticky: false,
        logoVariant: "mono",
        variant: "dark",
        className: styles.navbar
      }
    }
  }
}
