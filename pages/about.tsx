import { GetStaticProps } from 'next'
import { BreadcrumbJsonLd, NextSeo } from 'next-seo'
import AboutPageMainBanner from '~/layout/about/main-banner'
import { PageWithNavbarProps } from './_app'

import { Container } from 'react-bootstrap'
import CtaContact from '~/components/ctas/banner-cta-contact'
import AboutPageContent1AboutMe from '~/layout/about/content-1-about-me'
import AboutPageContent2AboutSite from '~/layout/about/content-2-about-site'
import styles from "~/styles/page/about.module.scss"
import { getUrlBaseName } from '~/util/url-helper'

function AboutPage() {
  // So that the navbar style is mounted properly.
  styles;

  let urlBase = getUrlBaseName();

  return (<>
    <NextSeo
      title="About Chris"
      description="Chris is a software engineer in Bandung area, specialized with full-stack web development. Chris makes website, optimize them, and insert text to places like this :). Learn more about Christianto in this page!"
      openGraph={{
        type: 'profile',
        profile: {
          firstName: "Gunawan",
          lastName: "Christianto",
          username: "chez14",
          gender: "male"
        }
      }}
    />
    <BreadcrumbJsonLd
      itemListElements={[
        {
          position: 0,
          name: "Home",
          item: `${urlBase.origin}`
        },
        {
          position: 1,
          name: "About",
          item: `${urlBase.origin}/about`
        }
      ]} />

    <AboutPageMainBanner />

    <div className="my-5">
      <AboutPageContent1AboutMe />
    </div>

    <div className="my-5">
      <Container>
        <CtaContact className={styles.ctaColor} title="Contact" description="Get in touch with me" ctaVariant='outline-dark' />
      </Container>
    </div>

    <div className="my-5">
      <AboutPageContent2AboutSite />
    </div>
  </>)
}

export default AboutPage


export const getStaticProps: GetStaticProps<PageWithNavbarProps> = async (ctx) => {
  return {
    props: {
      navbarProps: {
        logoVariant: "mono",
        variant: "light",
        className: styles.navbar
      }
    }
  }
}
