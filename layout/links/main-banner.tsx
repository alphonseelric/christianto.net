import React from 'react'
import { Container } from 'react-bootstrap'
import styles from "./main-banner.module.scss"


function LinksPageMainBanner() {
  return (
    <div className={styles.mainBanner}>
      <Container>
        <h1>Links</h1>
      </Container>
    </div>
  )
}

export default LinksPageMainBanner
