import { faYoutube } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image';
import { Col, Container, Row } from 'react-bootstrap';

import styles from "./home.module.scss";
import profilePict from "~/assets/home/profile.jpg";
import Link from 'next/link';

export interface HomeMainBannerInterface {
}

function HomeMainBanner({ }: HomeMainBannerInterface) {
  return (
    <div className={styles.mainBanner}>
      <Container>
        <Row className="align-items-center justify-content-center">
          <Col xs={10} md={3} className="align-self-start">
            <Image
              src={profilePict}
              alt="Chris's Profile."
              width={256} height={256}
              className="img-fluid rounded rounded-rect my-3 mx-auto d-block"
              quality={100}
              priority/>
          </Col>
          <Col xs={12} md={6} className={[styles.credentialAutoAdjust, "my-3"].join(" ")}>
            <p className="bg-primary text-dark p-2 px-3 h1 d-inline-block m-0">
              Gunawan <b>Christianto</b>
            </p>
            <p className="h3 py-3">
              Software engineer in <del>Bandung</del> area.
            </p>
            <p>
              Hi there, I'm Chris, also known as <code>chez14</code>. I'm currently working as Maintenance Manager
              at <Link href='https://dnartworks.co.id/' target="_blank" rel="noreferrer noopener">DNArtworks Indonesia</Link>. I mostly work on web stuff,
              especially backend, automation (build, test, deploy), and consulting things with client.
            </p>
            <p>Wanna know more about me? See my <Link href="/about">About page</Link>.</p>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default HomeMainBanner
