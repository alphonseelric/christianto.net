import { faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Link from 'next/link'
import { Button, Col, Row } from 'react-bootstrap'
import SectionHead from '~/components/section-head/section-head'

export interface HomeSectionLatestProject {
  title: string,
  titleClassname: string
}

function HomeSectionContact() {
  return (
    <>
      <SectionHead title="Contact Me">
        <Link href="/contact" passHref legacyBehavior>
          <Button variant="primary">
            See My Contact Info <FontAwesomeIcon icon={faArrowRight} />
          </Button>
        </Link>
      </SectionHead>
    </>
  )
}

export default HomeSectionContact
