import { faObjectGroup, faUser } from '@fortawesome/free-regular-svg-icons';
import { faHashtag } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import Link from 'next/link';
import { Badge, Card } from 'react-bootstrap';
import { When } from 'react-if';
import useProgressiveImage from '~/components/utils/use-progressive-image';
import { BlogPost } from '~/data/blogs/blog-post';

import styles from './blog-post.module.scss';

export interface BlogPostProps {
  post: BlogPost
}

function BlogPost({ post }: BlogPostProps) {

  let postDate = new Date(post.date);
  let blogFeaturedImage = useProgressiveImage(post.featuredImage?.source_url);

  return (
    <Card className={styles.postItem}>
      <When condition={!!post.featuredImage?.source_url}>
        <div className={classNames({ "post-image-container": true, "is-visible": !!blogFeaturedImage })} style={{ "--blog-featured-img": `url('${blogFeaturedImage}')` }} />
      </When>
      <Card.Body className={classNames({ 'post-imageless': !post.featuredImage?.source_url })}>

        {/* Post title */}
        <Link className="text-decoration-none text-light" href={post.link}>
          <Card.Title dangerouslySetInnerHTML={{ __html: post.title }} />
        </Link>

        <Card.Text as="div" className='small my-2'>
          Posted by <FontAwesomeIcon icon={faUser} /> {post.author.map((author) => <span key={author.link} ><Link href={author.link} target="_blank" rel="noopener noreferrer"> {author.name}</Link>,</span>)} on {postDate.toLocaleString()}
        </Card.Text>

        {/* Mini meta data listers */}
        <Card.Text as="div" className="post-taglists small">
          {/* Category lister */}
          {post.categories.map((ca) => <Link href={ca.link} target="_blank" rel="noopener noreferrer" key={ca.link}>
            <Badge bg='accent-a'><FontAwesomeIcon icon={faObjectGroup} /> {ca.name}</Badge>
          </Link>)}
          {/* tags Lister */}
          {post.tags.map((ca) => <Link href={ca.link} target="_blank" rel="noopener noreferrer" key={ca.link}>
            <Badge bg='secondary'><FontAwesomeIcon icon={faHashtag} /> {ca.name}</Badge>
          </Link>)}
        </Card.Text>

        {/* Post Excercept */}
        <Card.Text as="div" dangerouslySetInnerHTML={{ __html: post.excerpt }} />

        {/* Card button */}
        <Card.Text className="pt-3">
          <Link className={styles.postItemLinker} href={post.link} passHref legacyBehavior>
            {/* <Button target="_blank" rel="noopener noreferrer" variant='outline-primary'> */}
            Read Post
            {/* </Button> */}
          </Link>
        </Card.Text>
      </Card.Body>
    </Card>
  )
}

export default BlogPost
