import { faUser } from '@fortawesome/free-regular-svg-icons'
import { faHashtag, faObjectGroup } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Link from 'next/link'
import React from 'react'
import { Badge, Button, Col, Row } from 'react-bootstrap'
import { BlogPost } from '~/data/blogs/blog-post'

import styles from "./featured-blog.module.scss";

export interface FeaturedPostProps {
  post: BlogPost
}

function BlogPostFeaturedPost({ post }: FeaturedPostProps) {
  let postDate = new Date(post.date);
  return (
    <Row className={styles.blogFeatured}>
      <Col xs={12} md={6} lg={4}>
        <Link href={post.link} rel="noopener noreferrer" target="_blank" className="post-image">
          <img src={post.featuredImage?.source_url} alt={post.title} height={post.featuredImage?.height} width={post.featuredImage?.width} />
        </Link>
      </Col>
      <Col xs={12} md={6} lg={8} className="d-flex align-items-center">
        <div className="post-details">

          <Link className="text-decoration-none text-light" href={post.link}>
            <h3 className="h1" dangerouslySetInnerHTML={{ __html: post?.title }} />
          </Link>

          <div className='my-3'>
            Posted by <FontAwesomeIcon icon={faUser} /> {post.author.map((author) => <span key={author.link} ><Link href={author.link} target="_blank" rel="noopener noreferrer"> {author.name}</Link>,</span>)} on {postDate.toLocaleString()}
          </div>

          <div className="my-3 post-taglists">
            {post.categories.map((ca) => <Link href={ca.link} target="_blank" rel="noopener noreferrer" key={ca.link}>
              <Badge bg='accent-a'><FontAwesomeIcon icon={faObjectGroup} /> {ca.name}</Badge>
            </Link>)}
            {post.tags.map((ca) => <Link href={ca.link} target="_blank" rel="noopener noreferrer" key={ca.link}>
              <Badge bg='secondary'><FontAwesomeIcon icon={faHashtag} /> {ca.name}</Badge>
            </Link>)}
          </div>

          <div className="lead" dangerouslySetInnerHTML={{ __html: post?.excerpt }} />

          <div>
            <Link href={post.link} passHref legacyBehavior>
              <Button className={styles.btnCta} variant='outline-primary' rel="noopener noreferrer" target="_blank">Read Post</Button>
            </Link>
          </div>
        </div>
      </Col>
    </Row >
  )
}

export default BlogPostFeaturedPost
