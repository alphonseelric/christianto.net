import { faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Link from 'next/link'
import { Button, Row } from 'react-bootstrap'
import SectionHead from '~/components/section-head/section-head'

export interface HomeSectionLatestProject {
  title: string,
  titleClassname: string
}

function HomeSectionLatestProject() {
  return (
    <>
      <SectionHead title="Works and Projects">
        <Link href="/works" passHref legacyBehavior>
          <Button variant='primary'>
            See all projects/work <FontAwesomeIcon icon={faArrowRight} />
          </Button>
        </Link>
      </SectionHead>
      <Row>

      </Row>
    </>
  )
}

export default HomeSectionLatestProject
