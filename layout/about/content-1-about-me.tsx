import { Col, Container, Row } from 'react-bootstrap'
import ExternalLink from '~/components/utils/external-link'

function AboutPageContent1AboutMe() {
  return (
    <div className='my-5'>
      <Container>
        <Row>
          <Col xs={12} md={8}>

            <div>
              <h2 className='display-3'>Henlo, I'm <span className="text-accent-a">Chris</span>.</h2>
              <p className="fs-3">
                I'm a software engineer based on <del>Bandung</del>, Indonesia, specialized in full-stack web development.
              </p>
              <p>
                I'm currently working as a Maintenance Manager at <ExternalLink href='https://dnartworks.co.id/'
                  title='PT. DNArtworks Komunikasi Visual'>DNArtworks Indonesia</ExternalLink>.
                I help our clients with their requests and needs, translate it to technical requirements, and
                manage engineers to work on them. In addition, I manage the company's infrastructure as well.
              </p>

              <p>
                I have a weird hobby of checking many platform's API, learn how they implement the limiters, safe guards and more, then
                try it out by my self by running some weird, specific, and sometimes dumb experiments. This weird hobby
                of mine actually helps me to advise my clients when they want use said platform. One of the latest examples
                is how I helped a Property Management company run their backup pipeline for their directory app.
                I helped by making the reports, documentations and proposing the restore strategy. Not exactly web development
                related, right? But that's why I pick the Software Engineer title, because I love to program other stuff too!
              </p>

            </div>

            <div className='my-5'>
              <h2>Skillsets</h2>
              <p>Aha! Time for me to show off my weird sets.</p>

              <Row>
                <Col xs={12} md={4}>
                  <h4>Front-end</h4>
                  <ul>
                    <li>HTML, CSS, Js, SASS</li>
                    <li>React + MobX</li>
                    <li>Nextjs</li>
                    <li className='text-light text-opacity-50'>jQuery</li>
                    <li className='text-light text-opacity-50'>Bootstrap</li>
                  </ul>
                </Col>
                <Col xs={12} md={4}>
                  <h4>Back-end</h4>
                  <ul>
                    <li>CodeIgniter 3+4 (PHP)</li>
                    <li>FatFree Framework 3.6+ (PHP)</li>
                    <li>Express (Js/Ts)</li>
                    <li>Axum (Rust) — Learning</li>
                    <li>MySQL, MongoDB</li>
                  </ul>
                </Col>
                <Col xs={12} md={4}>
                  <h4>Other</h4>
                  <p>...but web-tech related, or maybe not?, you decide lah.</p>
                  <ul>
                    <li>WordPress</li>
                    <li>Apache, Nginx</li>
                  </ul>
                </Col>

              </Row>
              <Row>
                <Col xs={12} md={6}>
                  <h4>Other</h4>
                  <p>...but for anything else.</p>
                  <ul>
                    <li>Rust — learning</li>
                    <li>Grafana, Prometheus</li>
                    <li>Docker</li>
                    <li>GitLab CI/CD</li>
                    <li>Digital Ocean, Linode</li>
                    <li>Cloudflare</li>
                  </ul>
                </Col>
                <Col xs={12} md={6}>
                  <h4>OSes</h4>
                  <p>
                    Since some job descriptions sometime mention certain experience requirements with certain OS,
                    I'm adding this extra section.
                  </p>
                  <ul>
                    <li>Ubuntu (my current workstation)</li>
                    <li>Windows</li>
                    <li>Alpine Linux (docker image only)</li>
                  </ul>
                </Col>
              </Row>
            </div>

            <div className="my-5">
              <h3>Languages</h3>
              <ul>
                <li>
                  <p className="fw-bold m-0">Bahasa Indonesia</p>
                  <p>Native level</p>
                </li>
                <li>
                  <p className="fw-bold m-0">English</p>
                  <p>Business level</p>
                </li>
                <li>
                  <p className="fw-bold m-0">日本語</p>
                  <p>Just strarted to learn ✨</p>
                </li>
              </ul>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default AboutPageContent1AboutMe
