import classNames from 'classnames';
import { Col, Container, Row } from 'react-bootstrap';
import ExternalLink from '~/components/utils/external-link';

import styles from "./content-2.module.scss";



function AboutPageContent2AboutSite() {
  return (<>
    <div className={classNames([styles.aboutSite])} id='about-site'>
      <Container>
        <Row className="py-5">
          <Col>
            <h2 className="h1 band-title">
              About this site
            </h2>
          </Col>
        </Row>

      </Container>
    </div>

    <Container>
      <div className='my-5'>
        <Row>
          <Col xs={12} md={8}>
            <h3>Abstract</h3>
            <p>
              Just like my friends who happened to be engineers, they all have their own website that showcases their works.
              I have owned this domain ever since... 2015, and mostly use them to run some experiments.
              The apex domain was used to directly host my blog, just a single landing page, and is now hosting my properly
              made website—this.
            </p>
            <p>
              That being said, originally the site was made with Tailwind, but I'm not very confident with that solution because it feels
              like I have to create everything from scratch: typography, colors, animation, etc. I can't put that much effort into using it,
              so I decided to remake the site back, and just use any technology I already know will need low or "essentially-zero" maintenance :)
            </p>
            <p>The story behind this site, my branding, and more will be posted in a blogpost, so you can read more behind the scenes.</p>
          </Col>
          <Col xs={12} md={4}>
            <h3>Tech Stack</h3>
            <div>
              <ul>
                <li><b>Bootstrap</b> on Plong Theme.</li>
                <li><b>React-Bootstrap</b> for Boostrap Js Binding.</li>
                <li><b>Nextjs</b>, hosted on Vercel.</li>
                <li><b>Google Analytics</b>, for performance optimization tracker.</li>
              </ul>
            </div>
            <p>
              I made my own Bootstrap Theming called Plong, <ExternalLink href="https://ch14.me/plong?utm_source=about-page&utm_medium=mention">
                learn more about Plong here</ExternalLink>, blog posts are incoming.
            </p>

          </Col>
        </Row>
      </div>

      <div className="my-5">
        <Row>
          <Col xs={12} md={8}>
            <h3>Mini FAQ</h3>

            <div>
              <h4>Why the crossed "Bandung" part on the home page?</h4>
              <p>
                Bandung is a city in Indonesia that includes the surrounding districts too. When people refer to "Bandung" in conversation,
                they usually mean the city, not the districts. So when people actually meant the districts area, they sometimes jokingly
                corrected themself with "Bandung coret", crossed Bandung.
              </p>
              <p>
                <ExternalLink href="https://en.wikipedia.org/wiki/Bandung_(disambiguation)">Learn more about Bandung on Wikipedia (EN)</ExternalLink>.
              </p>
            </div>
            <div>
              <h4>Why do you use "this domain" when mentioning email?</h4>
              <p>
                To prevent automated email scans on my site. Adding my email on IANA PEN and domain WHOIS Info already make my inbox full with spam, before
                finally I switched to major email provider with better spam filtering. In addition to that, AI still sucks at understanding
                relative context, so...
              </p>
            </div>
            <div>
              <h4>Why the external API for your own site?</h4>
              <p>
                I'm very lazy maintaining JS stuffs. Everything feels as if its unstable, and rebuilding it might break after several
                weeks. If I want to update my data, I need to consistently maintain the deps, which makes me harder to be productive.
                If I store the data on my own external API, I can just update the data, and let old codebase update their thing on their own.
                No need to refactor things when I just want to update my portfolio, just because the plugin author didn't want to update
                their plugin to support new major release of X.
              </p>
              <p>
                Yes, dependabot exist for both <ExternalLink href="https://github.com/dependabot">GitHub</ExternalLink> and &nbsp;
                <ExternalLink href="https://gitlab.com/dependabot-gitlab/dependabot">GitLab</ExternalLink>, but to be able
                for them to run the auto-merge thing, you will need to maintain test suite, in which, will also break when new
                version released with no backward compatibility. I'll reconsider
                this approach when either I have more time and energy, or, the thing got calmer as its mature.
              </p>
            </div>
          </Col>
        </Row>
      </div>

    </Container>
  </>
  )
}

export default AboutPageContent2AboutSite
